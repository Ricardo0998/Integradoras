package com.orbita.innovacion.sva_escuela;

public class ListItem_Notificacion {
    private String nombre, apellido_P, apellido_M, grado, grupo, token;

    public ListItem_Notificacion(String nombre, String apellido_P, String apellido_M,String grado, String grupo, String Token) {
        this.nombre = nombre;
        this.apellido_P = apellido_P;
        this.apellido_M = apellido_M;
        this.grado = grado;
        this.grupo = grupo;
        this.token = Token;
    }

    public String getToken() {
        return token;
    }

    @Override
    public String toString() {
        return "Nombre: " + nombre + " " + apellido_P + " " + apellido_M + "\n" + "Grado: " + grado + grupo;
    }
}
